inject.define("blockio.blockio", [
	"blockio.config.config",
	"logApi.log",
    function(config, console) {

        var BlockIo = require('block_io');

        var blockio = new BlockIo(config.API_KEY, config.SECRET_PIN, config.version);

				console.log(config.API_KEY, config.SECRET_PIN, config.version);

        return blockio;
    }
]);
