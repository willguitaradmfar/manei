inject.define("manei.model.agent", [
    "mongoDB.mongoDB",
    function (mongoDB) {

        var schema = mongoDB.Schema({
            thread : String,
            imei : String,
            auth : String,
            log : {
              ts : Number,
              method : String,
              args : [String],
              stack : String              
            },
            dtcreate: {
                type: 'Date',
                default: Date.now
            },
            dtupdate: {
                type: 'Date',
                default: Date.now
            }
        });

        var model = mongoDB.model('Agent', schema);

        return model;
    }
]);
