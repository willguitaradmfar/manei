inject.define("manei.model.order", [
    "mongoDB.mongoDB",
    function (mongoDB) {

        var schema = mongoDB.Schema({
            personFrom: {
                type: mongoDB.Schema.ObjectId,
                ref: 'Person'
            },
            personTo: {
                type: mongoDB.Schema.ObjectId,
                ref: 'Person'
            },
            status: [{
                code: Number,
                label: String,
                dtcreate: {
                    type: 'Date',
                    default: Date.now
                }
            }],
            itens: [{
                from: String,
                to: String,
                code: String,
                amount: Number,
                description: String,
                date: Date,
                workflow: Number,
                checksum: String,
                table: String,
                accountID: String
            }],
            total: Number,
            IDHash: String,
            obs: String,
            to: String,
            dtcreate: {
                type: 'Date',
                default: Date.now
            }
        });

        schema.statics.findOrderByIDHash = function (IDHash) {
            return this.findOne({
                'IDHash': IDHash
            });
        };

        schema.statics.findOrderByIDPersonTo = function (id, cb) {
            return this.find({
                'personTo': id
            });
        };

        schema.statics.findOrderByIDPersonFrom = function (id, cb) {
            return this.find({
                'personFrom': id
            });
        };

        schema.statics.groupByPersonFromSumTotal = function (personFrom, cb) {
            return this.aggregate([{
                $match: {
                    personFrom: personFrom._id
                }
            }, {
                $group: {
                    _id: '$personFrom',
                    amount: {
                        $sum: '$total'
                    }
                }
            }], function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    cb(undefined, result);
                }
            });
        };

        var model = mongoDB.model('Order', schema);

        return model;
    }
]);
