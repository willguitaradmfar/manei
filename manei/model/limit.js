inject.define("manei.model.limit", [
    "mongoDB.mongoDB",
    function (mongoDB) {

        var schema = mongoDB.Schema({
            person: {
                type: mongoDB.Schema.ObjectId,
                ref: 'Person'
            },
            ammount: Number,
            status: {
                code: Number,
                label: String,
                dtcreate: {
                    type: 'Date',
                    default: Date.now
                }
            },
            dtcreate: {
                type: 'Date',
                default: Date.now
            }
        });

        schema.statics.sumLimitByPerson = function (person, cb) {
            return this.aggregate([{
                $match: {
                    person: person._id,
                    'status.code': {
                        $ne: 2
                    }
                }
            }, {
                $group: {
                    _id: '$person',
                    ammount: {
                        $sum: '$ammount'
                    }
                }
            }], function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    cb(undefined, result);
                }
            });
        };

        schema.statics.sumSaldoPagarByPerson = function (person, cb) {
            return this.aggregate([{
                $match: {
                    person: person._id,
                    'status.code': 2
                }
            }, {
                $group: {
                    _id: '$person',
                    ammount: {
                        $sum: '$ammount'
                    }
                }
            }], function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    cb(undefined, result);
                }
            });
        };

        var model = mongoDB.model('Limit', schema);

        return model;
    }
]);
