inject.define("manei.model.mobile", [
    "mongoDB.mongoDB",
    function (mongoDB) {

        var schema = mongoDB.Schema({
            phoneNumber: String,
            IMEI: String,
            keyLabel_pre: String,
            keyLabel_pos: String,
            address_pre: String,
            address_pos: String,
            confirmationCode: String,
            auth : String,
            device_token_gcm : String,
            device_token_apn : String,
            person: {
                type: mongoDB.Schema.ObjectId,
                ref: 'Person'
            },
            dtcreate: {
                type: 'Date',
                default: Date.now
            },
            dtupdate: {
                type: 'Date',
                default: Date.now
            }
        });

        schema.statics.findByKeyLabelPre = function (keyLabel_pre, cb) {
            return this.findOne({
                'keyLabel_pre': keyLabel_pre
            }).exec(cb);
        };

        schema.statics.findByKeyLabelPos = function (keyLabel_pos, cb) {
            return this.findOne({
                'keyLabel_pos': keyLabel_pos
            }).exec(cb);
        };

        schema.statics.findByKeyAddressPre = function (address_pre, cb) {
            return this.findOne({
                'address_pre': address_pre
            }).exec(cb);
        };

        schema.statics.findByKeyAddressPos = function (address_pos, cb) {
            return this.findOne({
                'address_pos': address_pos
            }).exec(cb);
        };

        schema.statics.findByIMEI = function (IMEI, cb) {
            return this.findOne({
                'IMEI': IMEI
            }).exec(cb);
        };

        schema.statics.findLastByPerson = function (personId, cb) {
            return this.findOne({
                'person': personId
            })
            .sort({dtcreate : 1})
            .exec(cb);
        };

        var model = mongoDB.model('Mobile', schema);

        return model;
    }
]);
