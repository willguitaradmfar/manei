inject.define("manei.routes.agent", [
    "logApi.log",
    "restifyWeb.restifyWeb",
    "manei.rule.agent",
    "manei.routes.util.util",
    "kueQueue.kueQueue",
    "kueQueue.utils.util",
    function (console, restifyWeb, agentRule, utilRouter, kueQueue, logQueue) {
        var self = {};

        var restify = require('restify');

        self.getAll = function (req, res, next) {
            console.log('getAllAgent');
            agentRule.resolveAgents()
                .then(function (agents) {
                    return res.send(agents);
                })
                .catch(function (err) {
                    console.trace(err);
                    return next(utilRouter.errorResponse(err));
                });
        };

        self.getPagination = function (req, res, next) {
            console.log('getPaginationAgent');
            agentRule.resolvePaginationAgents(req.params.skip, req.params.limit)
                .then(function (agents) {
                    return res.send(agents);
                })
                .catch(function (err) {
                    console.trace(err);
                    return next(utilRouter.errorResponse(err));
                });
        };

        self.getPaginationOrderBy = function (req, res, next) {
            console.log('getPaginationAgent');
            agentRule.resolvePaginationOrderByAgents(req.params.skip, req.params.limit, req.params.orderBy)
                .then(function (agents) {
                    return res.send(agents);
                })
                .catch(function (err) {
                    console.trace(err);
                    return next(utilRouter.errorResponse(err));
                });
        };

        self.getById = function (req, res, next) {
            console.log('getAgentById for', req.params.id);
            agentRule.resolveAgentById(req.params.id)
                .then(function (agent) {
                    return res.send(agent);
                })
                .catch(function (err) {
                    console.trace(err);
                    return next(utilRouter.errorResponse(err));
                });
        };

        self.save = function (req, res, next) {
            console.log('Salvando agent .... ');
            agentRule.save(req.body)
                .then(function (agent) {
                    console.log('Agent salved!!! ID:', agent._id);
                    return next();
                })
                .catch(function (err) {
                    console.trace(err);
                    return next(utilRouter.errorResponse(err));
                });
        };

        self.queue = function (req, res, next) {
            console.log('Queue agent .... ');

            req.body.imei = req.headers.imei;
            req.body.auth = req.headers.auth;

            var job = kueQueue.create('agent', req.body)
            .save(function (err) {
                if (err) {
                    console.trace(err);
                    return next(utilRouter.errorResponse(err));
                }
                console.log('Queued agent');
                next();
            });

            logQueue.logQueue(job);
        };

        restifyWeb.get('/agent/:id', utilRouter.authentication,
            self.getById);

        restifyWeb.get('/agent', utilRouter.authentication,
                self.getAll);

        restifyWeb.get('/agent/pagination/:skip/:limit', utilRouter.authentication,
                self.getPagination);

        restifyWeb.get('/agent/pagination/:skip/:limit/:orderBy', utilRouter.authentication,
                self.getPaginationOrderBy);

        restifyWeb.post('/agent', utilRouter.authentication,
                self.save,
                utilRouter.doneOK);

        restifyWeb.post('/agent/queue', utilRouter.authentication,
                self.queue,
                utilRouter.doneOK);

        return self;
    }
]);
