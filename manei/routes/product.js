inject.define("manei.routes.product", [
    "logApi.log",
    "restifyWeb.restifyWeb",
    "manei.rule.product",
    "manei.routes.util.util",
    "kueQueue.kueQueue",
    "kueQueue.utils.util",
    function (console, restifyWeb, productRule, utilRouter, kueQueue, logQueue) {
        var self = {};

        var restify = require('restify');

        self.getAll = function (req, res, next) {
            console.log('getAllProduct');
            productRule.resolveProducts()
                .then(function (products) {
                    return res.send(products);
                })
                .catch(function (err) {
                    console.trace(err);
                    return next(utilRouter.errorResponse(err));
                });
        };

        self.getPagination = function (req, res, next) {
            console.log('getPaginationProduct');
            productRule.resolvePaginationProducts(req.params.skip, req.params.limit)
                .then(function (products) {
                    return res.send(products);
                })
                .catch(function (err) {
                    console.trace(err);
                    return next(utilRouter.errorResponse(err));
                });
        };

        self.getPaginationOrderBy = function (req, res, next) {
            console.log('getPaginationProduct');
            productRule.resolvePaginationOrderByProducts(req.params.skip, req.params.limit, req.params.orderBy)
                .then(function (products) {
                    return res.send(products);
                })
                .catch(function (err) {
                    console.trace(err);
                    return next(utilRouter.errorResponse(err));
                });
        };

        self.getById = function (req, res, next) {
            console.log('getProductById for', req.params.id);
            productRule.resolveProductById(req.params.id)
                .then(function (product) {
                    return res.send(product);
                })
                .catch(function (err) {
                    console.trace(err);
                    return next(utilRouter.errorResponse(err));
                });
        };

        self.save = function (req, res, next) {
            console.log('Salvando product .... ');
            productRule.save(req.body)
                .then(function (product) {
                    console.log('Product salved!!! ID:', product._id);
                    return next();
                })
                .catch(function (err) {
                    console.trace(err);
                    return next(utilRouter.errorResponse(err));
                });
        };

        self.queue = function (req, res, next) {
            console.log('Queue product .... ');

            var job = kueQueue.create('product', req.body)
            .save(function (err) {
                if (err) {
                    console.trace(err);
                    return next(utilRouter.errorResponse(err));
                }
                console.log('Queued product');
                next();
            });

            logQueue.logQueue(job);
        };

        restifyWeb.get('/product/:id', utilRouter.authentication,
            self.getById);

        restifyWeb.get('/product', utilRouter.authentication,
                self.getAll);

        restifyWeb.get('/product/pagination/:skip/:limit', utilRouter.authentication,
                self.getPagination);

        restifyWeb.get('/product/pagination/:skip/:limit/:orderBy', utilRouter.authentication,
                self.getPaginationOrderBy);

        restifyWeb.post('/product', utilRouter.authentication,
                self.save,
                utilRouter.doneOK);

        restifyWeb.post('/product/queue', utilRouter.authentication,
                self.save,
                utilRouter.doneOK);

        return self;
    }
]);
