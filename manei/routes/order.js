inject.define("manei.routes.order", [
    "logApi.log",
    "restifyWeb.restifyWeb",
    "manei.rule.order",
    "manei.routes.util.util",
    "kueQueue.kueQueue",
    "kueQueue.utils.util",
    function (console, restifyWeb, orderRule, utilRouter, kueQueue, logQueue) {
        var self = {};

        var restify = require('restify');

        self.getAll = function (req, res, next) {
            console.log('getAllOrder');
            orderRule.resolveOrders()
                .then(function (orders) {
                    return res.send(orders);
                })
                .catch(function (err) {
                    console.trace(err);
                    return next(utilRouter.errorResponse(err));
                });
        };

        self.getPagination = function (req, res, next) {
            console.log('getPaginationOrder');
            orderRule.resolvePaginationOrders(req.params.skip, req.params.limit)
                .then(function (orders) {
                    return res.send(orders);
                })
                .catch(function (err) {
                    console.trace(err);
                    return next(utilRouter.errorResponse(err));
                });
        };

        self.getPaginationOrderByPersonTo = function (req, res, next) {
            console.log('getPaginationOrderByPersonTo');

            var idUser = req.headers.iduser;

            orderRule.resolvePaginationOrderByPersonTo(idUser, req.params.skip, req.params.limit, req.params.orderBy)
                .then(function (orders) {
                    return res.send(orders);
                })
                .catch(function (err) {
                    console.trace(err);
                    return next(utilRouter.errorResponse(err));
                });
        };

        self.getPaginationOrderByPersonFrom = function (req, res, next) {
            console.log('getPaginationOrderByPersonFrom');

            var idUser = req.headers.iduser;

            orderRule.resolvePaginationOrderByPersonFrom(idUser, req.params.skip, req.params.limit, req.params.orderBy)
                .then(function (orders) {
                    return res.send(orders);
                })
                .catch(function (err) {
                    console.trace(err);
                    return next(utilRouter.errorResponse(err));
                });
        };

        self.getByIDHash = function (req, res, next) {
            console.log('getOrderById for', req.params.IDHash);
            orderRule.resolveOrderByIDHash(req.params.IDHash)
                .then(function (order) {
                    return res.send(order);
                })
                .catch(function (err) {
                    console.trace(err);
                    return next(utilRouter.errorResponse(err));
                });
        };

        self.save = function (req, res, next) {
            console.log('Salvando order .... ');
            orderRule.save(req.body)
                .then(function (order) {
                    console.log('Order salved!!! ID:', order._id);
                    return next();
                })
                .catch(function (err) {
                    console.trace(err);
                    return next(utilRouter.errorResponse(err));
                });
        };

        self.accept = function (req, res, next) {
            console.log('accept order .... ');
            orderRule.accept(req.body)
                .then(function (order) {
                    return next();
                })
                .catch(function (err) {
                    console.trace(err);
                    return next(utilRouter.errorResponse(err));
                });
        };

        self.done = function (req, res, next) {
            console.log('done order .... ');
            orderRule.done(req.body)
                .then(function (order) {
                    return next();
                })
                .catch(function (err) {
                    console.trace(err);
                    return next(utilRouter.errorResponse(err));
                });
        };

        self.reject = function (req, res, next) {

            console.log('rejectOrder');

            orderRule.reject(req.body)
                .then(function (order) {
                    return next();
                }).catch(function (err) {
                    console.trace(err);
                    return next(utilRouter.errorResponse(err));
                });

        };

        restifyWeb.get('/order/:IDHash', utilRouter.authentication,
            self.getByIDHash);

        restifyWeb.get('/order', utilRouter.authentication,
            self.getAll);

        restifyWeb.get('/order/pagination/:skip/:limit', utilRouter.authentication,
            self.getPagination);

        restifyWeb.get('/order/pagination/personTo/:skip/:limit/:orderBy', utilRouter.authentication,
            self.getPaginationOrderByPersonTo);

        restifyWeb.get('/order/pagination/personFrom/:skip/:limit/:orderBy', utilRouter.authentication,
            self.getPaginationOrderByPersonFrom);

        restifyWeb.post('/order', utilRouter.authentication,
            self.save,
            utilRouter.doneOK);

        restifyWeb.post('/order/accept', utilRouter.authentication,
            self.accept,
            utilRouter.doneOK);

        restifyWeb.post('/order/done', utilRouter.authentication,
            self.done,
            utilRouter.doneOK);

        restifyWeb.post('/order/reject', utilRouter.authentication,
            self.reject,
            utilRouter.doneOK);

        restifyWeb.post('/order/queue', utilRouter.authentication,
                self.save,
                utilRouter.doneOK);

        return self;
    }
]);
