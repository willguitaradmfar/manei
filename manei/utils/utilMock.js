inject.define("manei.utils.utilMock", [
  "logApi.log",
    function (console) {
        var self = {};

        var crypto = require('crypto');

        var _ = require('lodash');

        function randomRange(min, max) {
            return ~~(Math.random() * (max - min + 1)) + min
        }

        self.md5 = function (str) {

            if(!str) return;

            if(typeof str != 'string') str = str.toString();
            var md5 = crypto.createHash('md5');
            md5.update(str);
            return md5.digest('hex');
        };

        self.generateKeyLabelBlockIO = function () {
            var str = _.toArray(arguments).join('');
            return self.md5(str);
        };

        /**
         * Gerador de codigo de confirmação aleatório
         * Gera 6 dígitos 000-000
         */
        self.generateConfirmationCode = function () {
            return '999-999'
        };

        /**
         * Parse de Numero de codigo de confirmação
         * Gera um obj com as partes do numero
         */
        self.parseConfirmationCode = function (str) {

            var pattern;

            if (/^(\d{3})-{0,1}(\d{3})$/.test(str)) {
                pattern = /^(\d{3})-{0,1}(\d{3})$/;
            }

            if (!pattern) return undefined;

            var prefix = str.replace(pattern, '$1');
            var sulfix = str.replace(pattern, '$2');

            return {
                prefix: prefix,
                sulfix: sulfix
            }
        };

        /**
         * Parse de Numero de telefone
         * Gera um obj com as partes do numero
         */
        self.parsePhoneNumber = function (str, country, ddd) {

            var pattern;

            if (/^\\+(\d{2})\s{0,1}(\d{2})\s{0,1}(\d{5})-{0,1}\s{0,1}(\d{4})$/.test(str)) {
                pattern = /^\\+(\d{2})\s{0,1}(\d{2})\s{0,1}(\d{5})-{0,1}\s{0,1}(\d{4})$/;
            }

            if (/^(\d{2})\s{0,1}(\d{2})\s{0,1}(\d{5})-{0,1}\s{0,1}(\d{4})$/.test(str)) {
                pattern = /^(\d{2})\s{0,1}(\d{2})\s{0,1}(\d{5})-{0,1}\s{0,1}(\d{4})$/;
            }

            if (/^(\d{2})\s{0,1}(\d{5})-{0,1}\s{0,1}(\d{4})$/.test(str)) {
                str = (country || '55') + str;
                pattern = /^(\d{2})\s{0,1}(\d{2})\s{0,1}(\d{5})-{0,1}\s{0,1}(\d{4})$/;
            }

            if (/^(\d{5})-{0,1}\s{0,1}(\d{4})$/.test(str)) {
                str = (ddd || '11') + str;
                str = (country || '55') + str;
                pattern = /^(\d{2})\s{0,1}(\d{2})\s{0,1}(\d{5})-{0,1}\s{0,1}(\d{4})$/;
            }

            if (!pattern) return undefined;

            var country = str.replace(pattern, '$1');
            var ddd = str.replace(pattern, '$2');
            var prefix = str.replace(pattern, '$3');
            var sulfix = str.replace(pattern, '$4');

            return {
                country: country,
                ddd: ddd,
                prefix: prefix,
                sulfix: sulfix
            }
        };

        return self;
    }
]);
