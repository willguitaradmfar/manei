inject.define("manei.rule.transaction", [
    "manei.model.mobile",
    "manei.model.person",
    "manei.model.transaction",
    "blockio.api" + env,
    "manei.utils.util",
    "manei.config.config",
    "logApi.log",
    "kueQueue.kueQueue",
    "kueQueue.utils.util",
    "manei.rule.person",
    "manei.rule.mobile",
    "manei.model.limit",
    "manei.rule.order",
    function (Mobile, Person, Transaction, apiBlockIO, util, config, console, kueQueue, logQueue, personRule, mobileRule, Limit, orderRule) {
        var self = {};

        var q = require('q');

        self.resolveTransactions = function () {
            return q.Promise(function (resolve, reject, notify) {
                Transaction.find(function (err, transactions) {
                    if (err) {
                        console.trace(err);
                        return reject(err);
                    }

                    if (!transactions) {
                        return reject('transactions indefinido');
                    }

                    return resolve(transactions);
                });

            });
        };

        self.resolvePaginationOrderByTransactions = function (skip, limit, orderBy) {
            return q.Promise(function (resolve, reject, notify) {

                Transaction.find()
                    .skip(skip)
                    .limit(limit)
                    .sort(orderBy)
                    .exec(function (err, transactions) {
                        if (err) {
                            console.trace(err);
                            return reject(err);
                        }

                        if (!transactions) {
                            return reject('transactions indefinido');
                        }

                        return resolve(transactions);
                    });

            });
        };

        self.resolvePaginationTransactions = function (skip, limit) {
            return q.Promise(function (resolve, reject, notify) {

                Transaction.find().skip(skip).limit(limit)
                    .exec(function (err, transactions) {
                        if (err) {
                            console.trace(err);
                            return reject(err);
                        }

                        if (!transactions) {
                            return reject('transactions indefinido');
                        }

                        return resolve(transactions);
                    });

            });
        };

        self.resolveTransactionById = function (id) {
            return q.Promise(function (resolve, reject, notify) {

                if (!id) return reject('Id não foi passado');

                Transaction.findById(id, function (err, transaction) {
                    if (err) {
                        console.trace(err);
                        return reject(err);
                    }

                    if (!transaction) {
                        return reject('Transaction não encontrada');
                    }

                    return resolve(transaction);
                });

            });
        };

        self.save = function (transaction) {
            return q.Promise(function (resolve, reject, notify) {

                if (!transaction) return reject('transaction não foi passado corretamente');

                new Transaction(transaction)
                    .save(function (err, transaction) {
                        if (err) {
                            console.trace(err);
                            return reject(err);
                        }
                        return resolve(transaction);
                    });

            });
        };

        var resolveTransactionsByPersonId = function (id) {
            return q.resolve({
                    _id: id
                })
                .then(mobileRule.resolveMobileByPerson)
                .then(function (mobile) {
                    return q.Promise(function (resolve, reject, notify) {
                        q.all([
                            apiBlockIO.get_transactions_with_address({
                                type: 'sent',
                                addresses: mobile.address_pre
                            }),
                            apiBlockIO.get_transactions_with_address({
                                type: 'received',
                                addresses: mobile.address_pre
                            })
                        ]).spread(function (sents, receiveds) {
                            var txs = [];

                            for (var i in sents.data.txs) {
                                txs.push(sents.data.txs[i]);
                            }

                            for (var i in receiveds.data.txs) {
                                txs.push(receiveds.data.txs[i]);
                            }

                            return resolve(txs);

                        });
                    });
                });
        };

        /*
         * Um hand para transformar address LTC em nome de usuário na base
         * baseado no seu label cadastrado no Blockio
         */
        self.resolveTransactionAddressToName = function (tx, _to, _from, _amount, _type) {
            return q.Promise(function (resolve, reject, notify) {
                q.all([
                    personRule.resolvePersonByAddress(_to),
                    personRule.resolvePersonByAddress(_from)
                ]).spread(function (_toUserID, _fromUserID) {
                    return resolve({
                        from: _fromUserID.name,
                        to: _toUserID.name,
                        amount: _amount,
                        date: new Date(tx.time * 1000),
                        txid: tx.txid,
                        confirmations: tx.confirmations,
                        type: _type
                    });
                }).catch(function (err) {
                    console.trace(err);
                    return resolve({
                        from: 'Não encontrado',
                        to: 'Não encontrado',
                        amount: _amount,
                        date: new Date(tx.time * 1000),
                        txid: tx.txid,
                        confirmations: tx.confirmations,
                        type: _type
                    });
                });
            });
        };

        /**
         * Pegar lista de transação no blockio
         */
        self.resolveTransactionsByPersonId = function (id) {
            return q.resolve(id)
                .then(resolveTransactionsByPersonId)
                .then(function (txs) {
                    return q.Promise(function (resolve, reject, notify) {
                        var _txsPromises = [];

                        for (var i in txs) {

                            var tx = txs[i];
                            var _to;
                            var _from;
                            var _amount;
                            var _type;

                            if (tx.amounts_sent) {
                                _to = tx.amounts_sent[0].recipient;
                                _from = tx.senders[0];
                                _amount = parseFloat(tx.amounts_sent[0].amount) / config.conversionFactor;
                                _type = 'sent';
                            } else if (tx.amounts_received) {
                                _from = tx.senders[0];
                                _to = tx.amounts_received[0].recipient;
                                _amount = parseFloat(tx.amounts_received[0].amount) / config.conversionFactor;
                                _type = 'received';
                            }
                            _txsPromises.push(self.resolveTransactionAddressToName(tx, _to, _from, _amount, _type));
                        }

                        q.all(_txsPromises)
                            .spread(function () {
                                return resolve(Array.prototype.slice.call(arguments));
                            }).catch(function (err) {
                                console.trace(err);
                                return reject(err);
                            });

                    });
                });
        };

        /**
         * Transaciona passando o Valor em Real e os cpfCnpjs dos envolvidos
         */
        self.createTransactionByCpfCnpjPre = function (fromPersonCpfCnpj, toPersonCpfCnpj, amountReal, orderID) {
            var fromPersonKeyLabel = util.generateKeyLabelBlockIO(fromPersonCpfCnpj, 'pre');
            var toPersonKeyLabel = util.generateKeyLabelBlockIO(toPersonCpfCnpj, 'pre');
            return createTransactionByLabelsPre(fromPersonKeyLabel, toPersonKeyLabel, amountReal, orderID);
        };

        /**
         * Transaciona passando o Valor em Real e os cpfCnpjs dos envolvidos
         */
        self.createTransactionByCpfCnpjPos = function (fromPersonCpfCnpj, toPersonCpfCnpj, amountReal, orderID) {
            var fromPersonKeyLabel = util.generateKeyLabelBlockIO(fromPersonCpfCnpj, 'pos');
            var toPersonKeyLabel = util.generateKeyLabelBlockIO(toPersonCpfCnpj, 'pos');
            return createTransactionByLabelsPos(toPersonKeyLabel, fromPersonKeyLabel, amountReal, orderID);
        };

        /**
         * Cria uma transação entrando com as pessoas envolvidas e o valor em Reais
         */
        var createTransactionByLabelsPre = function (fromPersonKeyLabel, toPersonKeyLabel, amountReal, orderID) {
            return q.Promise(function (resolve, reject, notify) {

                console.log('Converte o fator do negócio Manei (de Real para LTC)');
                /**
                 * Converte o fator do negócio Manei (de Real para LTC)
                 */
                var amountLTC = amountReal * config.conversionFactor;

                q.all([
                        personRule.resolvePersonByLabelPre(fromPersonKeyLabel),
                        personRule.resolvePersonByLabelPre(toPersonKeyLabel),
                        mobileRule.resolveMobileByLabelPre(fromPersonKeyLabel),
                        mobileRule.resolveMobileByLabelPre(toPersonKeyLabel),
                    ]).spread(function (fromPersonObj, toPersonObj, mobileFrom, mobileTo) {

                        new Limit({
                            person: fromPersonObj,
                            ammount: (amountReal * -1),
                            status: orderRule.statusOrder.ORDERTRANSACTION
                        }).save(function (err, limit) {
                            if (err) {
                                console.trace(err);
                                return reject(err);
                            }

                            if (!fromPersonObj || !toPersonObj) {
                                var msgtrace = ['Problema na busca de Person por Label'].join(' ');
                                console.trace(msgtrace);
                                return reject(msgtrace);
                            }

                            console.log('Cria a transação no blockio, com o valor ja convertido');
                            /**
                             * Cria a transação no blockio, com o valor ja convertido
                             */
                            apiBlockIO.withdraw_from_labels_to_labels(fromPersonKeyLabel, toPersonKeyLabel, amountLTC)
                                .then(function (transaction) {

                                    /**
                                     * Taxa de transação da rede LTC
                                     */
                                    var network_fee = parseFloat(transaction.data.network_fee);
                                    console.log('Taxa cobrada', network_fee);

                                    /**
                                     * Taxa de transação do blockio
                                     */
                                    var blockio_fee = parseFloat(transaction.data.blockio_fee);

                                    /**
                                     * nome da rede que transacionou
                                     */
                                    var network = transaction.data.network;

                                    /**
                                     * Após, grava na base de dados os dados de transação
                                     */
                                    new Transaction({
                                        personFrom: fromPersonObj,
                                        personTo: toPersonObj,
                                        amountLTC: amountLTC,
                                        amountReal: amountReal,
                                        transactionId: transaction.data.txid,
                                        feeNetwork: network_fee,
                                        feeBlockIO: blockio_fee,
                                        network: network,
                                        order: orderID
                                    }).save(function (err, transaction) {
                                        if (err) {
                                            console.trace(err);
                                            return reject(err);
                                        }

                                        return resolve({
                                            phoneNumberFrom: mobileFrom.phoneNumber,
                                            phoneNumberTo: mobileTo.phoneNumber,
                                            transaction: transaction
                                        });
                                    });
                                })
                                .catch(function (err) {
                                    console.trace(err);
                                    return reject(err);
                                });
                        });
                    })
                    .catch(function (err) {
                        console.trace(err);
                        return reject(err);
                    });
            });
        };

        /**
         * Cria uma transação entrando com as pessoas envolvidas e o valor em Reais
         */
        var createTransactionByLabelsPos = function (fromPersonKeyLabel, toPersonKeyLabel, amountReal, orderID) {
            return q.Promise(function (resolve, reject, notify) {

                console.log('Converte o fator do negócio Manei (de Real para LTC)');
                /**
                 * Converte o fator do negócio Manei (de Real para LTC)
                 */
                var amountLTC = amountReal * config.conversionFactor;

                q.all([
                        personRule.resolvePersonByLabelPos(fromPersonKeyLabel),
                        personRule.resolvePersonByLabelPos(toPersonKeyLabel),
                        mobileRule.resolveMobileByLabelPos(fromPersonKeyLabel),
                        mobileRule.resolveMobileByLabelPos(toPersonKeyLabel),
                    ]).spread(function (fromPersonObj, toPersonObj, mobileFrom, mobileTo) {
                        new Limit({
                            person: toPersonObj, // AQUI É INVERTIDO POIS É UMA CONTA PÓS
                            ammount: (amountReal * -1),
                            status: orderRule.statusOrder.ORDERTRANSACTION
                        }).save(function (err, limit) {
                            if (err) {
                                console.trace(err);
                                return reject(err);
                            }
                            if (!fromPersonObj || !toPersonObj) {
                                var msgtrace = ['Problema na busca de Person por Label'].join(' ');
                                console.trace(msgtrace);
                                return reject(msgtrace);
                            }

                            console.log('Cria a transação no blockio, com o valor ja convertido');
                            /**
                             * Cria a transação no blockio, com o valor ja convertido
                             */
                            apiBlockIO.withdraw_from_labels_to_labels(fromPersonKeyLabel, toPersonKeyLabel, amountLTC)
                                .then(function (transaction) {

                                    console.log('transação', transaction);

                                    /**
                                     * Taxa de transação da rede LTC
                                     */
                                    var network_fee = parseFloat(transaction.data.network_fee);
                                    console.log('Taxa cobrada', network_fee);

                                    /**
                                     * Taxa de transação do blockio
                                     */
                                    var blockio_fee = parseFloat(transaction.data.blockio_fee);

                                    /**
                                     * nome da rede que transacionou
                                     */
                                    var network = transaction.data.network;

                                    /**
                                     * Após, grava na base de dados os dados de transação
                                     */
                                    new Transaction({
                                        personFrom: fromPersonObj,
                                        personTo: toPersonObj,
                                        amountLTC: amountLTC,
                                        amountReal: amountReal,
                                        transactionId: transaction.data.txid,
                                        feeNetwork: network_fee,
                                        feeBlockIO: blockio_fee,
                                        network: network,
                                        order: orderID
                                    }).save(function (err, transaction) {
                                        if (err) {
                                            console.trace(err);
                                            return reject(err);
                                        }

                                        return resolve({
                                            phoneNumberFrom: mobileFrom.phoneNumber,
                                            phoneNumberTo: mobileTo.phoneNumber,
                                            transaction: transaction
                                        });
                                    });
                                })
                                .catch(function (err) {
                                    console.trace(err);
                                    return reject(err);
                                });
                        });
                    })
                    .catch(function (err) {
                        console.trace(err);
                        return reject(err);
                    });
            });
        };

        return self;
    }
]);
