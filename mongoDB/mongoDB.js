inject.define("mongoDB.mongoDB", [
    "mongoDB.config.config",
    "logApi.log",
    function (config, console) {

        var mongoose = require('mongoose');

        var connect = function () {
            var url = ['mongodb://', config.host, ':', config.port, '/', config.db].join('');
            console.log("Conectando mongo", url);
            mongoose.connect(url, {}, function (err) {
                if (err)
                {
                    console.error("ERR: ", err);
                    setTimeout(connect, 1000 * 5);
                    return;
                }
                console.log('connected Mongo', config.host, config.port);
            });
        };

        connect();

        return mongoose;
    }
]);
