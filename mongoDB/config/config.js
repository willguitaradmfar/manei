inject.define("mongoDB.config.config", [
    function () {
        var self = {};

        self.port = process.env.PORTMONGO || 27017;
        self.host = process.env.HOSTMONGO || 'localhost';
        self.db = process.env.DBMONGO || 'MANEI_DEMO';

        return self;
    }
]);
