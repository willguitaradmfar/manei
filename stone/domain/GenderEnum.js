inject.define("stone.domain.GenderEnum", [
    function () {
        var self = function () {};

        self.M = 'Masculino';
        self.F = 'Feminino';

        return self;
    }
]);
