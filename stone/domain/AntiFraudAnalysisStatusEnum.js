inject.define("stone.domain.AntiFraudAnalysisStatusEnum", [
    function () {
        var self = function () {};

        self.PendingFraudAnalysisRequirement = 'Pendente de análise de antifraude';
        self.FraudAnalysisRequirementSent = 'Pedido enviado para análise de antifraude';
        self.Approved = 'Aprovado';
        self.Reproved = 'Reprovado';
        self.PendingManualAnalysis = 'Pendente de análise manual';
        self.NoTransactionToAnalyse = 'Sem transação para análise';
        self.FraudAnalysisWithError = 'Erro na análise de antifraude';

        return self;
    }
]);
