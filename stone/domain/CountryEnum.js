inject.define("stone.domain.CountryEnum", [
    function () {
        var self = function () {};

        self.Brazil = 'Brasil';
        self.USA = 'Estado Unidos';
        self.Argentina = 'Argentina';
        self.Bolivia = 'Bolívia';
        self.Chile = 'Chile';
        self.Colombia = 'Colombia';
        self.Uruguay = 'Uruguai';
        self.Mexico = 'México';
        self.Paraguay = 'Paraguai';

        return self;
    }
]);
