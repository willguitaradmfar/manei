inject.define("stone.domain.OrderStatusEnum", [
    function () {
        var self = function () {};

        self.Opened = 'Aberto';
        self.Closed = 'Fechado';
        self.Paid = 'Pago';
        self.Overpaid = 'Pago com valor maior';
        self.Canceled = 'Cancelado';
        self.PartialPaid = 'Pago parcialmente';
        self.WithError = 'Com erro';
        self.Created = 'Criado';
        self.OrderNotAuthorized = 'Pedido não autorizado';
        self.OrderNotPaid = 'Pedido não pago';

        return self;
    }
]);
