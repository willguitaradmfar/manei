inject.define("stone.domain.PersonTypeEnum", [
    function () {
        var self = function () {};

        self.Person = 'Pessoa física';
        self.Company = 'Pessoa jurídica';

        return self;
    }
]);
