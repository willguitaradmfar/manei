inject.define("stone.domain.BuyerCategoryEnum", [
    function () {
        var self = function () {};

        self.Normal = 'Normal';
        self.Plus = 'Plus';

        return self;
    }
]);
