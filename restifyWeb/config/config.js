inject.define("restifyWeb.config.config", [
    function() {
        var self = {};

        self.appName = "MANEI";
        self.version = "0.0.1";
        self.appPort = process.env.PORT || 3000;

        return self;
    }
]);
