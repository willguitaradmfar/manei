var app = angular.module('manei');

app.factory('Agent', function ($q, Http, ShowMessage, Queue, Domain) {

    var queue = new Queue(['agent', 'queue'].join('/'));

    queue.service();

    var Agent = function () {
      Domain.apply(arguments);
      this.domainName = 'agent';
    };

    Agent.prototype = Object.create(Domain.prototype);  

    Agent.prototype.queue = function () {
        queue.add(this);
    };
    return Agent;
});
