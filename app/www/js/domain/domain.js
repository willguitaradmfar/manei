var app = angular.module('manei');

app.factory('Domain', function ($q, Http, ShowMessage, Queue, Storage) {

    var Domain = function () {
        this.domainName = 'domain';
    };

    Domain.prototype.create = function (domain) {
        if (!domain) return;
        for (var i in domain) {
            this[i] = domain[i];
        }
        return this;
    };

    Domain.prototype.resolveLoad = function (id, primaryKey) {

        var cache = new Storage(['cache', this.domainName, 'list'].join('.'));

        var $this = this;
        return $q(function (resolve, reject) {
            Http.get([$this.domainName, id].join('/'))
                .then(function (domain) {
                    return resolve(domain.data);
                }).catch(function (err) {
                    console.error(err);
                    var domains = cache.get();
                    for (var i in domains) {
                        var domain = domains[i];
                        if (domain[primaryKey || '_id'] == id) {
                            return resolve(domain);
                        }
                    }
                });
        });
    };

    Domain.prototype.load = function (id, primaryKey) {
        var $this = this;
        Domain.prototype.resolveLoad.call(this, id, primaryKey)
            .then(function (domain) {
                $this.create(domain);
            });
    };

    Domain.prototype.resolveAll = function () {
        var cache = new Storage(['cache', this.domainName, 'list'].join('.'));

        var $this = this;
        return $q(function (resolve, reject) {
            Http.get($this.domainName)
                .then(function (domains) {
                    var _domains = [];
                    for (var i in domains.data) {
                        var domain = domains.data[i];
                        _domains.push(new Domain().create(domain));
                    }
                    cache.save(_domains);
                    return resolve(_domains);
                }).catch(function (err) {
                    console.error(err);
                    var _domains = [];
                    var domains = cache.get();
                    for (var i in domains) {
                        var domain = domains[i];
                        _domains.push(new Domain().create(domain));
                    }
                    return resolve(_domains);
                });
        });
    };

    Domain.prototype.save = function () {
        return Http.post(this.domainName, this);
    };

    Domain.prototype.saveCache = function () {
        var cache = new Storage(['cache', this.domainName, '_'].join('.'));
        cache.save(this);
    };

    Domain.prototype.loadCache = function () {
        var cache = new Storage(['cache', this.domainName, '_'].join('.'));
        var d = cache.get();
        this.create(d);
    };

    Domain.prototype.removeCache = function () {
        var cache = new Storage(['cache', this.domainName, '_'].join('.'));
        cache.remove();
    };

    Domain.prototype.resolveNextPage = function (limit, orderBy) {

        var cache = new Storage(['cache', this.domainName, 'list'].join('.'));

        if (!this.skip) this.skip = 0;

        var $this = this;

        return $q(function (resolve, reject) {
            Http.get([$this.domainName, 'pagination', $this.skip, limit, orderBy].join('/'))
                .then(function (domains) {

                    var _domains = [];
                    for (var i in domains.data) {
                        var domain = domains.data[i];
                        _domains.push(new Domain().create(domain));
                    }
                    cache.save(_domains);
                    return resolve(_domains);
                }).catch(function (err) {
                    console.error(err);
                    var _domains = [];
                    var domains = cache.get();
                    for (var i in domains) {
                        var domain = domains[i];
                        _domains.push(new Domain().create(domain));
                    }
                    return resolve(_domains);
                });

            $this.skip += limit;
        });
    };

    return Domain;
});
