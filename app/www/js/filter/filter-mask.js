var app = angular.module('manei');

app.filter('mask', function (TimeAgo) {
    return function (text, args) {
        var formated = '';
        var atraso = 0;
        for(var i in args){
          if(args[i] == 9){
            formated += text[i-atraso];
          }else{
            formated += args[i];
            atraso++;
          }
        }
        return formated;
    };
});
