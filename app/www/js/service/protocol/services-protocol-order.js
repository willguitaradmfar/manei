var app = angular.module('manei');

app.service('ProtocolOrder', function (Authentication, Order, ZIP, Adler32) {

    var user = new Authentication().getUser();

    var zip = new ZIP();

    var adler32 = new Adler32();

    var ProtocolOrder = function () {};

    ProtocolOrder.prototype.decode = function (str) {

        if (!str) return;

        console.log('Decode', str);

        str = zip.decompress(str);

        var order = new Order();

        var regexProduct = /^(\d{1,})$/;

        var fieldsChecksum = str.split('$');

        if (fieldsChecksum.length != 2) return;

        var checksum = fieldsChecksum[1];

        var fields = fieldsChecksum[0].split('|');

        var IDHash = fields.pop();
        var amount = fields.pop();
        var _to = fields.pop();
        var _from = fields.pop();

        for (var i in fields) {
            var code = fields[i];

            if (regexProduct.test(code)) {
                var item = {};
                item.to = _to;
                item.from = _from;
                item.code = code;
                order.addItem(item).then(function () {});
            }
        }

        order.IDHash = IDHash;
        order.to = _to;
        order.total = amount / 100;

        var _checksum = adler32.encode(makeProtocol(order));

        console.log(_checksum, checksum);
        console.log(makeProtocol(order), fieldsChecksum[0]);

        if (_checksum != checksum) {
            console.error('Checksum diferente', _checksum, checksum);
            return;
        }

        return order;
    };

    var makeProtocol = function (order) {

        var protocol = [];

        var amount = 0;
        var to;
        var _from;

        for (var i in order.itens) {
            var item = order.itens[i];
            protocol.push(item.code);

            if (item.amount)
                amount += item.amount;

            if (!to) {
                to = item.to;
            }

            if (to != item.to) {
                throw 'Existe destino diferente nessa Ordem';
            }

            if (!_from) {
                _from = item.from;
            }

            if (_from != item.from) {
                throw 'Existe origem diferente nessa Ordem';
            }
        }

        if (order.to)
            to = order.to;

        if (order.from)
            _from = order.from;

        protocol.push(_from);
        protocol.push(to);

        if (order.total) {
            amount = order.total;
        }

        var _amount = Math.round(amount * 100);
        protocol.push(_amount);

        protocol.push(order.IDHash);

        return protocol.join('|');
    };

    ProtocolOrder.prototype.encode = function (order) {
        for (var i in order.itens) {
            var item = order.itens[i];
            if (item.amount == undefined) {
                throw 'Existe Item sem valor definido';
            }
        }

        var protocol = makeProtocol(order);

        var checksum = adler32.encode(protocol);

        protocol = protocol + '$' + checksum;

        protocol = zip.compress(protocol);

        return protocol;
    };

    return ProtocolOrder;
});
