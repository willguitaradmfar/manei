var app = angular.module('manei');

app.service('Authentication', function ($state, Person, ShowMessage, ManeiConfig) {

    var Authentication = function () {
        this.personFactory = new Person();
        this.personFactory.loadCache();

    };

    Authentication.prototype.verify = function () {
        this.personFactory.loadCache();
        if (!this.personFactory || !this.personFactory.id) {
            $state.go('immersion.welcome');            
        }
    };

    Authentication.prototype.getUser = function () {
        this.verify();
        return this.personFactory;
    };

    return Authentication;
});
