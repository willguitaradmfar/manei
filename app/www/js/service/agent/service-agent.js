var app = angular.module('manei');

app.service('AgentService', function (ManeiConfig, Http, Queue, Agent) {

    var methods = "assert clear count debug dir dirxml error exception group groupCollapsed groupEnd info log markTimeline profile profileEnd table time timeEnd timeStamp trace warn".split(" ");
    var console = (window.console = window.console || {});
    var logCount = 0;
    window.onerror = function (msg, url, line) {
        if (msg && url) console.error(msg, url, (line ? "Line: " + line : ""));
    };

    function getStack(err) {
        var sa = err.stack.split('at ')[2];
        var regex = /.*\/(.*)\/(.*\.js):(\d*):(\d*).*\n\s{0,}/;
        return sa.replace(regex, '$1/$2:$3:$4');
    };

    function sendConsoleLog(method, args, stack) {
        try {
            var imei = localStorage.getItem("device.imei");

            var obj = {
                index: logCount,
                method: method,
                ts: Date.now(),
                args: args,
                stack: stack
            };
            var agent = new Agent().create(obj);
            agent.queue();

        } catch (e) {}
    }
    for (var x = 0; x < methods.length; x++) {
        (function (m) {
            var orgConsole = console[m];
            console[m] = function () {
                try {
                    sendConsoleLog(m, Array.prototype.slice.call(arguments), getStack(new Error()));
                    if (orgConsole) orgConsole.apply(console, arguments);
                } catch (e) {}
            };
        })(methods[x]);
    }

    return

});
