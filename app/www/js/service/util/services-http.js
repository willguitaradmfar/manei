var app = angular.module('manei');

app.service('Http', function ($http, ManeiConfig, $ionicLoading, Storage, $q) {

    var cacheMobile = new Storage(['cache', 'mobile', '_'].join('.'));
    var cachePerson = new Storage(['cache', 'person', '_'].join('.'));

    var updateHead = function () {
        var mobile = cacheMobile.get();
        var person = cachePerson.get();

        if (mobile && person && mobile.IMEI && mobile.auth && person.id) {
            $http.defaults.headers.common['IMEI'] = mobile.IMEI || 'SEM IMEI';
            $http.defaults.headers.common['AUTH'] = mobile.auth || 'SEM AUTH';
            $http.defaults.headers.common['IDUSER'] = person.id || 'SEM USER ID';
        }
    };

    return {
        get: function (service, disableLoading) {
            return $q(function (resolve, reject) {
                updateHead();

                if (!disableLoading) $ionicLoading.show();

                $http.get(ManeiConfig.host + service)
                    .then(function (data) {
                        try {
                            return resolve(data);
                        } catch (err) {
                            if (!disableLoading) $ionicLoading.hide();
                            return reject(err);
                        } finally {
                            if (!disableLoading) $ionicLoading.hide();
                        }
                    })
                    .catch(function (err) {
                        if (!disableLoading) $ionicLoading.hide();

                        if (!err.data && !err.statusText) {
                            return reject('Erro com conexão de Rede');
                        }

                        if (err.data && err.data.message) {
                            return reject(err.data.message);
                        }
                    });
            });
        },
        post: function (service, data, disableLoading) {
            return $q(function (resolve, reject) {
                updateHead();

                if (!disableLoading) $ionicLoading.show();

                $http.post(ManeiConfig.host + service, data)
                    .then(function (data) {
                        try {
                            return resolve(data);
                        } catch (err) {
                            if (!disableLoading) $ionicLoading.hide();
                            return reject(err);
                        } finally {
                            if (!disableLoading) $ionicLoading.hide();
                        }
                    })
                    .catch(function (err) {
                        if (!disableLoading) $ionicLoading.hide();

                        if (!err.data && !err.statusText) {
                            return reject('Erro com conexão de Rede');
                        }

                        if (err.data && err.data.message) {
                            return reject(err.data.message);
                        }
                    });
            });
        }
    };
});
