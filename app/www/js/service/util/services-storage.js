var app = angular.module('manei');

app.service('Storage', function () {
    var Storage = function (_key) {
      this.key = _key;
    };

    Storage.prototype.save = function (list) {
        var list = JSON.stringify(list, function (key, array) {
            if (key == "$$hashKey") return false;
            return array;
        });
        localStorage.setItem(this.key, list);
    };

    Storage.prototype.get = function () {
        var list = localStorage.getItem(this.key);
        return JSON.parse(list);
    };

    Storage.prototype.moveTo = function (_key) {
        var list = localStorage.getItem(this.key);
        localStorage.setItem(_key, list);
        localStorage.removeItem(this.key);
    };

    Storage.prototype.remove = function (_key) {
        localStorage.removeItem(this.key);
    };

    return Storage;
});
