var app = angular.module('manei');

app.service('Util', function () {
    return {
        md5: function (str) {
            return ['MD5(', str, ')'].join('');
        }
    };
});
