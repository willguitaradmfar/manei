angular.module('manei', [
    'ionic',    
    'ui.mask'
])

        .run(function ($ionicPlatform) {
            $ionicPlatform.ready(function () {

                if (window.cordova && window.cordova.plugins.Keyboard) {
                    cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                    cordova.plugins.Keyboard.disableScroll(true);

                }
                if (window.StatusBar) {
                    StatusBar.styleDefault();
                }
            });
        })

        .config(function ($stateProvider, $urlRouterProvider) {

            $stateProvider
                    .state('app', {
                        url: '/app',
                        abstract: true,
                        templateUrl: 'templates/store/menu.html',
                        controller: 'AppCtrl'
                    })

                    .state('app.home', {
                        url: '/home',
                        cache: false,
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/store/requestList.html',
                                controller: 'RequestListCtrl'
                            }
                        }
                    })

                    .state('app.profile', {
                        url: '/profile',
                        cache: false,
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/store/profile.html',
                                controller: 'ProfileStoreCtrl'
                            }
                        }
                    })
                    .state('app.transactions', {
                        url: '/transactions',
                        cache: false,
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/store/transactions.html',
                                controller: 'TransactionsCtrl'
                            }
                        }
                    })

                    .state('app.requestList', {
                        url: '/requestList',
                        cache: false,
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/store/requestList.html',
                                controller: 'RequestListCtrl'
                            }
                        }
                    })
                    
                    .state('app.requestListGroupByAccount', {
                        url: '/requestListGroupByAccount',
                        cache: false,
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/store/requestListGroupByAccount.html',
                                controller: 'RequestListGroupByAccountCtrl'
                            }
                        }
                    })

                    .state('app.requestOrder', {
                        url: '/requestOrder/:id',
                        cache: false,
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/store/requestOrder.html',
                                controller: 'RequestOrderCtrl'
                            }
                        }
                    })

                    .state('app.catalog', {
                        url: '/catalog',
                        cache: false,
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/store/catalog.html',
                                controller: 'CatalogCtrl'
                            }
                        }
                    })
                    .state('app.catalogDetail', {
                        url: '/catalogDetail/:id',
                        cache: false,
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/store/catalogDetail.html',
                                controller: 'CatalogDetailCtrl'
                            }
                        }
                    })

                    .state('app.order-capture', {
                        url: '/order-capture',
                        cache: false,
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/store/order-capture.html',
                                controller: 'OrderAuthorizationCtrl'
                            }
                        }
                    })

                    .state('app.balance', {
                        url: '/balance',
                        cache: false,
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/store/balance.html',
                                controller: 'BalanceCtrl'
                            }
                        }
                    })

                    .state('immersion', {
                        url: '/init',
                        abstract: true,
                        templateUrl: 'templates/store/immersion/init.html'
                    })

                    .state('immersion.welcome', {
                        //url: '/welcome',
                        cache: false,
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/store/immersion/welcome.html',
                                controller: 'WelcomeCtrl'
                            }
                        }
                    })

                    .state('immersion.phonenumber', {
                        //url: '/phonenumber',
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/store/immersion/phonenumber.html',
                                controller: 'PhoneNumberCtrl'
                            }
                        }
                    })

                    .state('immersion.codesms', {
                        //url: '/codesms',
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/store/immersion/codesms.html',
                                controller: 'CodeSMSCtrl'
                            }
                        }
                    })

                    .state('immersion.profile-step-1', {
                        //url: '/profile',
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/store/immersion/profile-step-1.html',
                                controller: 'ProfileCtrl'
                            }
                        }
                    })

                    .state('immersion.profile-step-2', {
                        //url: '/profile',
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/store/immersion/profile-step-2.html',
                                controller: 'ProfileCtrl'
                            }
                        }
                    })

                    .state('immersion.profile-step-3', {
                        //url: '/profile',
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/store/immersion/profile-step-3.html',
                                controller: 'ProfileCtrl'
                            }
                        }
                    });

            // redireciona para tela home se autenticado
            // controlado com localStorage

            $urlRouterProvider.otherwise('/app/home');

        });
