var app = angular.module('manei');

app.directive('resolveProductName', function (Storage) {

    var storage = new Storage('estabelecimento.catalog');
    return {
        restrict: 'A',
        scope: {
            resolveProductName: '=resolveProductName'
        },
        link: function (scope, element, attr) {
            if (!scope.resolveProductName) return;

            var elem = element[0];

            var products = storage.get();

            products = products.filter(function (o) {
                if (o.code == scope.resolveProductName) {
                    return o;
                }
            });

            if (products.length == 0) {
                elem.innerText = scope.resolveProductName;
                return
            };

            var product = products[0];

            elem.innerText = product.description;

        }
    };
});

app.directive('resolveProductAmount', function (Storage) {

    var storage = new Storage('estabelecimento.catalog');
    return {
        restrict: 'A',
        scope: {
            resolveProductAmount: '=resolveProductAmount'
        },
        link: function (scope, element, attr) {
            if (!scope.resolveProductAmount) return;

            var elem = element[0];

            var products = storage.get();

            products = products.filter(function (o) {
                if (o.code == scope.resolveProductAmount) {
                    return o;
                }
            });

            if (products.length == 0) {
                elem.innerText = 'R$ ?';
                return
            };

            var product = products[0];

            elem.innerText = ['R$', product.amount.toFixed(2)].join(' ');

        }
    };
});
