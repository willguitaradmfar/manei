var app = angular.module('manei');

app.directive('resolveUserName', function (Http) {
    return {
        restrict: 'A',
        scope: {
            resolveUserName: '=resolveUserName'
        },
        link: function (scope, element, attr) {
            if (!scope.resolveUserName) return;

            var elem = element[0];

            Http.get('/getUserName/' + scope.resolveUserName)
                .then(function (data) {
                    elem.innerText = data.data.name;
                })
                .catch(function (err) {
                    elem.innerText = scope.resolveUserName;
                });
        }
    };
});
