var app = angular.module('manei');

app.controller('OrderResumeCtrl', function ($scope, $ionicHistory, ProtocolOrder, Order, Authentication) {

    $ionicHistory.nextViewOptions({
        disableBack: true
    });

    $scope.order = new Order();
    $scope.order.get();

    //Assumindo que as vendas são Pos-pago/Crédito
    var person = new Authentication().getUser();
    $scope.balanceLimite = person.balanceLimite;

    var protocolOrder = new ProtocolOrder();
    $scope.qr = protocolOrder.encode($scope.order);

    var order = new Order();
    order.save();
});
