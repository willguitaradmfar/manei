var app = angular.module('manei');

app.controller('CatalogCtrl', function ($scope, $stateParams, $state, Storage, Product) {

    $scope.add = function (catalog) {
        catalog.time = new Date().getTime();
        $scope.catalogs.push(catalog);

        var product = new Product();
        product.create(catalog);
        product.queue();
    };

    $scope.load = function () {
        var productFactory = new Product();
        productFactory.resolveAll()
                .then(function (catalogs) {
                    $scope.catalogs = catalogs;
                })
                .finally(function () {
                    $scope.$broadcast('scroll.refreshComplete');
                })
                .catch(function (err) {

                });
    };

    $scope.load();

});
