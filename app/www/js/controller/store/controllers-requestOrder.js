var app = angular.module('manei');

app.controller('RequestOrderCtrl', function ($scope, $stateParams, $state, Http, Authentication, Queue, Order, Storage, ShowMessage, Product) {

    var user = new Authentication().getUser();

    $scope.sendProcess = false;

    console.log('RequestOrderCtrl');

    var storageCatalog = new Storage('estabelecimento.catalog');

    var productFactory = new Product();

    $scope.accept = function (order) {
        order = new Order().create(order);
        productFactory.attachAmount(order.itens)
                .then(function (o) {
                    order.accept();
                    $scope.sendProcess = true;
                });
    };

    $scope.done = function (order) {
        order = new Order().create(order);
        productFactory.attachAmount(order.itens)
                .then(function (o) {
                    order.done();
                    $scope.sendProcess = true;
                });
    };

    $scope.reject = function (order) {
        order.reject();
        $scope.sendProcess = true;
    };

    $scope.id = $stateParams.id;

    $scope.load = function () {
        $scope.order = new Order();
        $scope.order.load($scope.id, 'IDHash');
        $scope.$broadcast('scroll.refreshComplete');
    };

    $scope.load();

});
