var app = angular.module('manei');

app.controller('BalanceCtrl', function ($scope, TimeAgo, Order, ShowMessage, Person) {

    $scope.load = function () {
        var person = new Person();
        person.resumoTransactionReceitaPos()
                .then(function (resumo) {
                    $scope.resumos = resumo;

                    $scope.resumos.map(function (o) {
                        o.date = new Date(o._id.year, (o._id.month - 1), 01);
                        return o;
                    });

                })
                .finally(function () {
                    $scope.$broadcast('scroll.refreshComplete');
                });
    };

    $scope.load();

});
