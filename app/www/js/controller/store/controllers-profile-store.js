var app = angular.module('manei');

app.controller('ProfileStoreCtrl', function ($scope, $stateParams, $state, Authentication, Mobile, ShowMessage) {

    $scope.user = new Authentication().getUser();

    var mobile =  new Mobile();
    mobile.loadCache();
    $scope.mobile = mobile;

    $scope.sair = function () {
        ShowMessage.showMessage("Deseja desabilitar esse celular ?", function () {
            $scope.user.removeCache();
            $state.go('immersion.phonenumber');
        }, function () {

        });
    };

});
