inject.define("redisCache.redisCache", [
	"redisCache.config.config",
	"logApi.log",
    function(config, console) {

		var redis = require('redis');

        var client = redis.createClient(config.port, config.host);

        client.on('connect', function(err) {
        	if(err){
        		console.trace("ERR: ", err);
        		return;
        	}
		    console.log('connected Redis', config.host, config.port);
		});

        return client;
    }
]);
