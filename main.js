inject = require('injectjs').inject;

console.debug = console.log;

inject.loadLibrary = function (path) {
    console.log('##', 'DEPENDENCIA', path, '##');
    require(path);
};

env = process.env.Mock ? 'Mock' : '';

inject.src = './';
//inject.debug = true;
inject.define("Main", [
    "manei.rule.transaction",
    "manei.utils.LZString",
    "manei.routes.mobile",
    "manei.routes.person",
    "manei.consumers.util.sms",
    "manei.consumers.smsImersao",
    "manei.consumers.transaction",
    "manei.routes.transaction",
    "manei.routes.order",
    "manei.routes.agent",
    "manei.routes.product",
    "manei.consumers.agent",    
    "manei.consumers.person",
    "manei.routes.mobile",
    function (rule, LZString) {
        var self = {};
        return self;
    }
]);
