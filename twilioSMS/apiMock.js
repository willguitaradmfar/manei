var q = require('q');

inject.define("twilioSMS.apiMock", [
    "twilioSMS.twilio",
    "twilioSMS.config.config",
    "logApi.log",
    function (twilio, config, console) {
        var self = {};

        self.sendMessage = function (to, body) {
            return q.Promise(function (resolve, reject, notify) {
                console.log('Mock ... Enviando SMS para', to);
                resolve();
            });
        };

        return self;
    }
]);
