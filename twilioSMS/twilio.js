inject.define("twilioSMS.twilio", [
	"twilioSMS.config.config",
    function(config) {
    	
		var client = require('twilio')(config.accountSid, config.authString);

        return client;
    }
]);
