var q = require('q');

inject.define("twilioSMS.api", [
    "twilioSMS.twilio",
    "twilioSMS.config.config",
    "logApi.log",
    function (twilio, config, console) {
        var self = {};

        self.sendMessage = function (to, body) {
            return q.Promise(function (resolve, reject, notify) {
                console.log('Enviando SMS para', to);
                twilio.messages.create({
                    body: body,
                    to: to,
                    from: config.phoneNumber
                }, function (err, message) {
                    if (err) {
                        console.trace(err);
                        return reject(err);
                    }
                    resolve(message);
                });
            });
        };

        return self;
    }
]);
